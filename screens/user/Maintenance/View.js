import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  ActivityIndicator,
  Dimensions,
  LayoutAnimation,
  Platform,
  UIManager,
  Alert,
  ScrollView,
  TouchableOpacity,
  Picker,
  Animated
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Header from '../../common/Header';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import AsyncStorage from '@react-native-community/async-storage';
import { API_URL } from '../../../global/config';
import { TouchableRipple, Snackbar, FAB, TextInput } from 'react-native-paper';
import { connect } from 'react-redux';
import { getThemeColors } from '../../../global/themes';
import Loader from '../../common/Loader';
import { globalStyles } from '../../../global/styles';
import Ripple from 'react-native-material-ripple';
import Load from '../../common/Load';
import { RefreshControl } from 'react-native';


import Feather from 'react-native-vector-icons/Feather';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
class CarView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      contentLoading: false,
      visible: false,
      success: false,
      showLoading: true,
      recordSummery: {},
      listArray: [],
      days: '',
      recordId: null,
      type: '',
      listRefreshing: false,
      expanded: false,
      selectedIndex: '',
      visiblee: false
    };

    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  async componentDidMount() {
    // let userInfo = await AsyncStorage.getItem('userInfo');
    // if (userInfo) {
    //   userInfo = JSON.parse(userInfo);
    // this.setState({
    //   title: 'JEEP',
    // });
    // this.detailLoadByCar();
  }

  detailLoadByCar = () => {
    let bodyData = JSON.stringify({
      car_type: this.state.type,
    });
    console.log(`${API_URL}detailLoadByCar`, bodyData);
    fetch(`${API_URL}detailLoadByCar`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: bodyData,
    })
      .then((res) => res.json())
      .then((response) => {
        if (response.status) {
          //   LayoutAnimation.configureNext({
          //     duration: 450,
          //     create: {
          //       type: LayoutAnimation.Types.easeInEaseOut,
          //       property: LayoutAnimation.Properties.opacity,
          //     },
          //     update: {type: LayoutAnimation.Types.easeInEaseOut},
          //   });
          //   console.log(response.data);
          this.setState({
            showLoading: false,
            listRefreshing: false,
          });

          if (response.data.length > 0) {
            this.setState({
              listArray: response.data,
            });
          }
        }
      })
      .catch((err) => {
        console.log(err);
        this.setState({ showLoading: false });
      });
  };

  showSnackbar = (message, status = false) => {
    this.setState({
      visible: true,
      success: status,
      message: message,
    });
  };

  deleteThisCar = (id) => {
    Alert.alert(
      'Are you sure?',
      'You want to delete this record?.',
      [
        { text: 'Cancel', onPress: () => null },
        {
          text: 'OK',
          onPress: () => {
            this.setState({ showLoading: true });
            fetch(`${API_URL}loadDelete`, {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({ id: id }),
            })
              .then((res) => res.json())
              .then((response) => {
                this.setState({ showLoading: false });
                if (response.status) {
                  this.detailLoadByCar();
                  this.showSnackbar(response.status_text, true);
                } else {
                  this.showSnackbar(response.status_text);
                }
              })
              .catch((err) => {
                this.setState({ showLoading: false });
              });
          },
        },
      ],
      { cancelable: false },
    );
  };



  _menu = [];

  setMenuRef = (ref) => {
    this._menu = ref;
  };

  hideMenu = (item, index) => {
    this._menu[index].hide()
  };

  showMenu = (item, index) => {
    this._menu[index].show()
  };

  render() {
    const param = this.props.route.params;
    if (param !== undefined) {
      if (param.screen !== undefined) {
        console.log(param);
        this.setState({ title: param.title, type: param.type },
          () => {
            this.detailLoadByCar();
          },
        );
        param.screen = undefined;
      }
    }

    const { colors } = this.props;
    return (
      <>
        <Header title={this.state.title} back={true} />
        {/* <> */}
        <View style={styles.container(colors.backgroundColor)}>
          <ScrollView
            style={styles.container}
            refreshControl={
              <RefreshControl
                tintColor={'lime'}
                onRefresh={() => {
                  this.setState({ listRefreshing: true },
                    () => {
                      this.detailLoadByCar();
                    },
                  );
                }}
                refreshing={this.state.listRefreshing}
              />
            }>
            <View style={{ paddingHorizontal: 12, marginTop: 10 }}>
              <Animated.FlatList
                data={this.state.listArray}
                renderItem={({ item, index }) => (
                  <View
                    style={[
                      this.props.style,
                      {
                        marginVertical: 5,
                        borderWidth: 1,
                        borderColor: '#999999',
                        elevation: 2,
                        borderRadius: 15,
                        backgroundColor: colors.cardColor,
                      },
                    ]}>
                    {/* <Text>{JSON.stringify(item.unit_id)}</Text> */}
                    <View
                      style={(this.state.expanded === true && this.state.selectedIndex === index) ? styles.cardExpand : styles.cardNormal}>
                      <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                        <Text style={[styles.textStyle]}>BA No. : </Text>
                        <TouchableOpacity
                          style={{ marginBottom: 'auto' }}
                          onPress={() => this.props.navigate('Details', { id: item.id, screen: 'Dashboard' })}>
                          <Text style={[styles.textStyle, { color: '#78A3E8' }]}>
                            {item.ba_no}
                          </Text>
                        </TouchableOpacity>

                        <View style={{ marginLeft: 'auto', flexDirection: 'row' }}>
                          <TouchableOpacity onPress={() => { this.toggleExpand(index) }}
                            style={{ paddingHorizontal: 5, paddingBottom: 5, }}>
                            <Feather size={22} name="info" color="#a4b5c5" />
                          </TouchableOpacity>

                          <Menu ref={(menu) => { this._menu[index] = menu }}
                            style={{ backgroundColor: colors.secondaryColor }}
                            button={
                              <TouchableOpacity onPress={() => { this.showMenu(item, index) }}
                                style={{ paddingHorizontal: 5, paddingBottom: 5 }}>
                                <MaterialCommunityIcons size={22} name="dots-vertical" color="#a4b5c5" />
                              </TouchableOpacity>
                            }>
                            <MenuItem onPress={() => {
                              this.props.navigation.navigate('Form', { id: item.id, title: 'Edit Load', screen: 'Index' });
                              this.hideMenu(item, index)
                            }}>
                              <Text style={{ color: colors.textColor }}>Edit</Text>
                            </MenuItem>
                            <MenuItem
                              onPress={() => {
                                this.deleteThisCar(item.id)
                                this.hideMenu(item, index)
                              }} >
                              <Text style={{ color: colors.textColor }}>Delete</Text>
                            </MenuItem>
                          </Menu>

                        </View>
                      </View>
                      <Text style={{color: colors.cardTextColor, fontSize: 20,fontWeight: 'bold'}}>
                        {item.model.name}
                      </Text>
                    </View>
                    {this.state.expanded && (
                      this.state.selectedIndex === index ?
                        <View style={styles.childCard(colors.cardColor)}>
                          <Text style={styles.textStyle}>Unit : {item.unit.name}</Text>
                          <Text style={styles.textStyle}>BA NO : {item.ba_no}</Text>
                          <Text style={styles.textStyle}>KM : {item.km}</Text>
                          <Text style={styles.textStyle}>Type : {item.type.name}</Text>
                          <Text style={styles.textStyle}>Class : {item.cl.name}</Text>
                          <Text style={styles.textStyle}>Engine : {item.engine}</Text>
                          <Text style={styles.textStyle}>Other : {item.other}</Text>
                        </View>
                        : null
                    )}
                  </View>
                )} />
            </View>
          </ScrollView>
        </View>
        {/* </> */}
        <Snackbar
          visible={this.state.visible}
          duration={2000}
          onDismiss={() => {this.setState({visible: false})}}
          style={
            this.state.success
              ? { backgroundColor: colors.alertSuccess }
              : { backgroundColor: colors.alertDanger }
          }>
          {this.state.message}
        </Snackbar>
        {this.state.showLoading && <Loader color={colors.primaryColor} />}
      </>
    );
  }
  toggleExpand = (index) => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({ expanded: !this.state.expanded, selectedIndex: index });
  };
}

const mapStateToProps = (state) => {
  var theme = getThemeColors(state.theme);
  return { colors: theme };
};

export default connect(mapStateToProps)(CarView);

const styles = StyleSheet.create({
  container: (bgColor) => ({
    flex: 1,
    backgroundColor: bgColor,
  }),
  cardStyle: (bgColor) => ({
    borderRadius: 15,
    backgroundColor: bgColor,
    paddingHorizontal: 20,
    paddingVertical: 10,
    width: '49%',
    borderWidth: 1,
    elevation: 5,
    marginBottom: 10,
  }),
  cardHeading: (color) => ({
    fontSize: 20,
    textAlign: 'center',
    color: color,
  }),
  cardNumber: (color) => ({
    paddingTop: 5,
    fontSize: 25,
    textAlign: 'center',
    fontWeight: 'bold',
    color: color,
  }),
  head: (color, bgColor) => ({
    height: 40,
    backgroundColor: bgColor,
    color: color,
  }),
  text: (color) => ({ margin: 6, color: color, fontSize: 16, opacity: 0.9 }),


  textStyle: {
    color: '#6c6e70',
    fontSize: 16,
    fontWeight: 'bold',
  },
  textBlack: (color) => ({
    color: color,
    fontSize: 16,
    opacity: 0.6,
  }),
  textWhite: (cardTextColor) => ({
    color: cardTextColor,
    fontSize: 16,
    opacity: 0.8,
  }),
  badgeStyle: (bgColor) => ({
    color: '#fff',
    fontSize: 13,
    backgroundColor: bgColor,
    paddingHorizontal: 6,
    paddingVertical: 1.2,
    borderRadius: 8,
    textTransform: 'uppercase',
    elevation: 1.2,
  }),
  cardNormal: {
    // backgroundColor: bgColor,
    padding: 10,
    // borderRadius: 15,
  },
  childCard: (bgColor) => ({
    backgroundColor: bgColor,
    padding: 10,
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
  }),
  cardExpand: {
    backgroundColor: '#014CA7',
    padding: 10,
    borderTopRightRadius: 15,
    borderTopLeftRadius: 15,
  },
});
